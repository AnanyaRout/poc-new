import * as moment from "moment";
import { NotaryScheduleExceptions, EventDuration, UserSlots } from "../../shared/db/models";
export const eventSlots = async (allEvents, date) => {
    const daysNumbers = {
        "MONDAY": 1,
        "TUESDAY": 2,
        "WEDNUSDAY": 3,
        "THURSDAY": 4,
        "FRIDAY": 5,
        "SATURDAY": 6,
        "SUNDAY": 7
    };
    const endDate = date.split("-");
    // tslint:disable-next-line:radix
    const year = parseInt(endDate[0]);
    // tslint:disable-next-line:radix
    const month = parseInt(endDate[1]) - 1;
    const duration = allEvents.duration;
    const start = moment().startOf('month').set({ year, month, hour: 0, minute: 0, second: 0, millisecond: 0 });
    const end = moment().endOf('month').set({ year, month, hour: 0, minute: 0, second: 0, millisecond: 0 });
    let days = [];
    allEvents.NotarySchedules.map((each) => {
        const tmp = start.clone().day(daysNumbers[each.day]);
        const eDay = start.clone().day(1).add(7, 'days');
        if (tmp.isAfter(start, 'd')) {
            const spots = [];

            const sTime = moment(tmp.format('YYYY-MM-DD') + each.startTime, "YYYY-MM-DD HH:mm");
            const eTime = moment(tmp.format('YYYY-MM-DD') + each.endTime, "YYYY-MM-DD HH:mm");
            const endTime = moment(tmp.format('YYYY-MM-DD') + each.startTime, "YYYY-MM-DD HH:mm").add(duration, "minutes");
            while (sTime.isBefore(eTime) && endTime.isSameOrBefore(eTime)) {
                spots.push({
                    startTime: sTime.format(),
                    availability: true
                })
                sTime.add(duration, "minutes");
                endTime.add(duration, "minutes");
            }
            if(tmp.isSameOrAfter(moment())) {
                days.push({ date: tmp.format('YYYY-MM-DD'), spots });
            }
        }
        while (tmp.isBefore(end) && eDay.isSameOrBefore(end)) {
            const spots = [];

            tmp.add(7, 'days');
            eDay.add(7, 'days');
            const sTime = moment(tmp.format('YYYY-MM-DD') + each.startTime, "YYYY-MM-DD HH:mm");
            const eTime = moment(tmp.format('YYYY-MM-DD') + each.endTime, "YYYY-MM-DD HH:mm");
            const endTime = moment(tmp.format('YYYY-MM-DD') + each.startTime, "YYYY-MM-DD HH:mm").add(duration, "minutes");
            while (sTime.isBefore(eTime) && endTime.isSameOrBefore(eTime)) {
                spots.push({
                    startTime: sTime.format(),
                    availability: true
                })
                sTime.add(duration, "minutes");
                endTime.add(duration, "minutes");
            }
            if (tmp.isSameOrBefore(end) && tmp.isSameOrAfter(moment())) {
                days.push({ date: tmp.format('YYYY-MM-DD'), spots });
            }
        }
    })

    const exceptionSchedules:any = await EventDuration.findOne({
        where: {
            deletedAt: null,
            uuid: allEvents.uuid
        },
        include: [NotaryScheduleExceptions,UserSlots]
    });
    const notAvailableDates = exceptionSchedules.NotaryScheduleExceptions.filter((each)=> {
            return each.availableOnThisDate === false;
    }).map((each)=> moment(each.dateTime).format('YYYY-MM-DD'));

    // days.filter((each)=> each.d)
    let notAvailableSlots = exceptionSchedules.NotaryScheduleExceptions.filter((each)=> {
        return each.availableOnThisDate === true;
    }).map((each)=> moment(each.dateTime).format());

    const bookedUserSlots= exceptionSchedules.UserSlots.filter((each)=> {
        return each.deletedAt === null;
    }).map((each)=> moment(each.dateTime).format());
    notAvailableSlots = notAvailableSlots.concat(bookedUserSlots);
    notAvailableSlots = [...new Set(notAvailableSlots)];
    days = days.filter((each)=> {
        const d1= notAvailableDates.filter((e1)=> {
            return e1===each.date;
        });
        if(d1.length === 0) {
            return each;
        }
    });

    days= days.map((each)=> {
        each.spots= each.spots.filter((e1)=> {
            const d1= notAvailableSlots.filter((e2)=> {
                return e2===e1.startTime;
            });
            if(d1.length === 0) {
                return e1;
            }
        });
        return {
            date: each.date,
            spots: each.spots
        };
    })
    return days;
}