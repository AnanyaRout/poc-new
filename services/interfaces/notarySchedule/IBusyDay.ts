export interface IBusyDay {
    dateTime: Date,
    availableOnThisDate: boolean
}