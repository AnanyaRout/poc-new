export interface ICreateNotarySchedule {
    schedules: [{
        day: string,
        startTime: string,
        endTime: string
    }],
    durations: number,
    eventName: string
}