export interface IUpdateDaySchedule {
    schedules: [{
        day: string,
        startTime: string,
        endTime: string
    }],
}