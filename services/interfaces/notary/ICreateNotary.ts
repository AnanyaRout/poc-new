export interface ICreateNotary {
    name: string,
    email: string
}