import { Request, Response } from "express";
import app from ".";
import { sequelize } from "../shared/db/sequelize";
import router from "./api/routes";

const PORT = process.env.PORT || 3000;
try {
  sequelize.authenticate();
  // tslint:disable-next-line:no-console
  console.log('Connection has been established successfully.');
} catch (error) {
  // tslint:disable-next-line:no-console
  console.error('Unable to connect to the database:', error);
}
if (process.env.SYNC === "true") {
  sequelize.sync({ force: true });
}

app.get("/", (_req: Request, res: Response) => {
  res.send("hello..");
})
// appRoutes(app);
app.use("/api/v1", router);

// tslint:disable-next-line:no-console
app.listen(PORT, () => console.log(`listening on port ${PORT}!`));
