import * as dotenv from 'dotenv';
import { expressApp } from '../expressApp';

// Add extra middleware to the app
const result = dotenv.config();
if (result.error) {
  throw result.error;
}
export default expressApp;
