import router from "./routes";

export default function appRoutes(app) {
    app.use("/api/v1", router);
};


