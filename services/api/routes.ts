import { Router } from "express";
import { controllerHandler } from "../../shared/lib/controllerHandler";
import { createNotary } from "../controllers/notary/createNotary";
import { createNotarySchedule } from "../controllers/notarySchedule/createNotarySchedule";
import { createUser } from "../controllers/user/createUser";
import { bookSlot } from "../controllers/user/bookSlot";
import { busyDay } from "../controllers/notarySchedule/busyDay";
import { updateDaySchedule }  from "../controllers/notarySchedule/updateDaySchedule";
import { getNotaryEvent } from "../controllers/notarySchedule/getNotaryEvent";
import { getOneNotaryEventSlots } from "../controllers/notarySchedule/getOneNotaryEventSlots";
import { notaryWorkSheet } from "../controllers/notary/notaryWorkSheet";
import { userAppointments } from "../controllers/user/userAppointments";
const router = Router();

router.post("/notary",controllerHandler({
    controller: createNotary
}));

router.post("/notary/schedule/:uuid",controllerHandler({
    controller: createNotarySchedule
}));

router.get("/notary/event/:uuid",controllerHandler({
    controller: getNotaryEvent,
    options: { transaction: false },
}));

router.get("/event/:uuid",controllerHandler({
    controller: getOneNotaryEventSlots,
    options: { transaction: false },
}));

router.post("/user",controllerHandler({
    controller: createUser
}));

router.post("/book/:uuid/:eventId",controllerHandler({
    controller: bookSlot
}));

router.post("/busy/:uuid",controllerHandler({
    controller: busyDay
}));

router.put("/notary/schedule/:uuid",controllerHandler({
    controller: updateDaySchedule
}));

router.get("/work/sheets/:uuid",controllerHandler({
    controller: notaryWorkSheet,
    options: { transaction: false },
}));

router.get("/user/appointments/:uuid",controllerHandler({
    controller: userAppointments,
    options: { transaction: false },
}));
export default router;
