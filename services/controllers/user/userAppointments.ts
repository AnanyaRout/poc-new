import { User, UserSlots } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";

export const userAppointments = async (params: IControllerParams<{}>) => {

    const details = await User.findOne({
        where: {
            uuid: params.args.params.uuid,
        },
        include: [UserSlots]
    });
    if (!details) {
        throw new HttpBadRequest("User not found.");
    }
    return {
        message: "Successful",
        payload: details
    }
}