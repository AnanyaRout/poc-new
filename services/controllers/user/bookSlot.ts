import * as moment from "moment";
import { EventDuration, NotarySchedules, User, UserSlots } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
import { eventSlots } from "../../functions/eventSlots";
import { IBusyDay } from "../../interfaces/notarySchedule/IBusyDay";

export const bookSlot = async (params: IControllerParams<IBusyDay>) => {
    const transaction = params.transaction;
    const inputs = params.input;
    const user = await User.findOne({
        where: {
            uuid: params.args.params.uuid,
            deletedAt: null
        }
    });

    if (!user) {
        throw new HttpBadRequest("User not found.");
    }
    const allEvents: any = await EventDuration.findOne({
        where: {
            uuid: params.args.params.eventId
        },
        include: [ NotarySchedules]
    });

    if (!allEvents) {
        throw new HttpBadRequest("Event not found.");
    }
    const userSlot = await UserSlots.findAll({
        where: {
            userUuid: params.args.params.uuid
        }
    });
    if (userSlot.length !== 0) {
        await UserSlots.destroy({
            where: {
                userUuid: params.args.params.uuid,
                eventUuid: params.args.params.eventId
            },
            force: true
        })
    }
    let dateTime;

    if (moment(inputs.dateTime).isBefore(moment())) {
        throw new HttpBadRequest("Please enter a latest date.");
    }
    const days = await eventSlots(allEvents, moment(inputs.dateTime).format('YYYY-MM-DD'));

    let present = false;
    days.map((each) => {
        dateTime = each.spots.filter((each1) => {
            return each1.startTime === inputs.dateTime;
        })
        if (dateTime.length !== 0) {
            present = true;
        }
    })
    if (present === false) {
        throw new HttpBadRequest("This date time is not valid");
    }
    await UserSlots.create({
        dateTime: inputs.dateTime,
        notaryUuid: allEvents.notaryUuid,
        eventUuid: allEvents.uuid,
        userUuid: user.uuid
    })
    await transaction.commit();

    return {
        message: "Successfull",
        payload: days
    }
}