import { User } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { ICreateUser } from "../../interfaces/user/ICreateUser";

export const createUser = async (params: IControllerParams<ICreateUser>) => {
    const inputs = params.input;
    const transaction = params.transaction;
    const user = await User.create({
        name: inputs.name,
        email: inputs.email
    }, { transaction });

    await transaction.commit();

    return {
        message: "Created",
        payload: user
    }
}