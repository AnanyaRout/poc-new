import { EventDuration, NotarySchedules } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
import { eventSlots } from "../../functions/eventSlots";
export const getOneNotaryEventSlots = async (params: IControllerParams<{}>) => {
    const allEvents: any = await EventDuration.findOne({
        where: {
            uuid: params.args.params.uuid
        },
        include: {
            model: NotarySchedules,
        }
    });

    if (!allEvents) {
        throw new HttpBadRequest("Event not found.");
    }
    // const endDate = params.args.queryString.date.split("-");
    const days = await eventSlots(allEvents,params.args.queryString.date);

    return {
        message: "Successful",
        payload: {
            eventName: allEvents.eventName,
            duration: allEvents.duration,
            days
        }
    }
}