import { EventDuration, Notary, NotarySchedules } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
import { IUpdateDaySchedule } from "../../interfaces/notarySchedule/IUpdateDaySchedule";

export const updateDaySchedule = async (params: IControllerParams<IUpdateDaySchedule>) => {
    const inputs = params.input;
    const transactions = params.transaction;
    const notary: any = await Notary.findOne({
        where: {
            uuid: params.args.params.uuid,
            deletedAt: null
        },
        include: {
            model: EventDuration,
            where: {
                uuid: params.args.queryString.eventUuid
            },
            include: [NotarySchedules]
        }
    });

    if (!notary) {
        throw new HttpBadRequest("Notary with this uuid doesn't exists.");
    }
    const schedulesToCreate = [];
    const schedulesToUpdate = []

    inputs.schedules.map((each) => {
        const existDay = notary.EventDurations[0].NotarySchedules.find((each1) => {
            return each1.day === each.day;
        });
        if (existDay) {
            schedulesToUpdate.push({
                uuid: existDay.uuid,
                day: each.day,
                startTime: each.startTime,
                endTime: each.endTime
            });
        } else {
            schedulesToCreate.push(each);
        }
    })

    if (schedulesToUpdate.length !== 0) {
        schedulesToUpdate.map(async (each) => {
            await NotarySchedules.update({
                startTime: each.startTime,
                endTime: each.endTime
            }, {
                where: {
                    uuid: each.uuid,
                    day: each.day
                }
            });
        })
    }
    if (schedulesToCreate.length !== 0) {
        await NotarySchedules.bulkCreate(schedulesToCreate.map((each) => {
            return {
                notaryUuid: notary.uuid,
                eventUuid: params.args.queryString.eventUuid,
                day: each.day,
                startTime: each.startTime,
                endTime: each.endTime
            }
        }));
    }

    await transactions.commit();

    return {
        message: "Ok",
        payload: await Notary.findOne({
            where: {
                uuid: params.args.params.uuid
            },
        })
    }
}