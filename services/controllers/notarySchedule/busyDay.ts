import { EventDuration, NotaryScheduleExceptions, NotarySchedules } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
import { eventSlots } from "../../functions/eventSlots";
import { IBusyDay } from "../../interfaces/notarySchedule/IBusyDay";
import * as moment from "moment";

export const busyDay = async (params: IControllerParams<IBusyDay>) => {
    const transaction = params.transaction;
    const inputs = params.input;
    const allEvents: any = await EventDuration.findOne({
        where: {
            uuid: params.args.params.uuid
        },
        include: {
            model: NotarySchedules,
        }
    });

    let dateTime;
    if (!allEvents) {
        throw new HttpBadRequest("Event not found.");
    }

    const notaryScheduleException = await NotaryScheduleExceptions.findOne({
        where: {
            deletedAt: null,
            dateTime: moment(inputs.dateTime).toDate(),
            availableOnThisDate: inputs.availableOnThisDate,
            notaryUuid: allEvents.notaryUuid,
            eventUuid: allEvents.uuid
        }
    });

    if(notaryScheduleException) {
        throw new HttpBadRequest("Can't save replicate datas.");
    }
    if(moment(inputs.dateTime).isBefore(moment())) {
        throw new HttpBadRequest("Please enter a latest date.");
    }
    const days = await eventSlots(allEvents, moment(inputs.dateTime).format('YYYY-MM-DD'));

    if (inputs.availableOnThisDate === true) {
        days.map((each) => {
            each.spots.find((each1) => {
                if(each1.startTime === moment(inputs.dateTime).format()) {
                    dateTime = each1;
                }
            })
        })
    } else {
        dateTime = days.find((each) => {
            return each.date === moment(inputs.dateTime).format('YYYY-MM-DD');
        })
    }
    if (!dateTime) {
        throw new HttpBadRequest("This date time is not valid");
    }
    await NotaryScheduleExceptions.create({
        dateTime: moment(inputs.dateTime).toDate(),
        availableOnThisDate: inputs.availableOnThisDate,
        notaryUuid: allEvents.notaryUuid,
        eventUuid: allEvents.uuid
    })
    await transaction.commit();

    return {
        message: "Successfull",
    }
}