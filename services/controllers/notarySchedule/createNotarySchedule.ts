import { Notary, NotarySchedules, EventDuration } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
import { ICreateNotarySchedule } from "../../interfaces/notarySchedule/ICreateNotarySchedule";

export const createNotarySchedule = async (params: IControllerParams<ICreateNotarySchedule>) => {
    const inputs = params.input;
    const transactions = params.transaction;
    const notary = await Notary.findOne({
        where: {
            uuid: params.args.params.uuid,
            deletedAt: null
        },
    });

    if (!notary) {
        throw new HttpBadRequest("Notary with this uuid doesn't exists.");
    }

    const event = await EventDuration.findOne({
        where: {
            notaryUuid: notary.uuid,
            eventName: inputs.eventName,
            duration: inputs.durations
        }
    });
    if(event) {
        throw new HttpBadRequest("Event with this name and durations already exists.")
    }
    const notaryScheduleDatas = inputs.schedules.map((each) => {
        return {
            day: each.day,
            startTime: each.startTime,
            endTime: each.endTime,
            notaryUuid: params.args.params.uuid,
        }
    });

    await EventDuration.create({
        eventName: inputs.eventName,
        duration: inputs.durations,
        notaryUuid: notary.uuid,
        NotarySchedules: notaryScheduleDatas
    },{
        include: [NotarySchedules]
    });

    await transactions.commit();

    return {
        message: "Created",
        payload: await Notary.findOne({
            where: {
                uuid: params.args.params.uuid
            },
        })
    }
}