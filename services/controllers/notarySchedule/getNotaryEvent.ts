import { EventDuration, Notary } from "../../../shared/db/models";
import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";
export const getNotaryEvent = async (params: IControllerParams<{}>) => {
    const filter: any = {
        deletedAt: null,
        availability: true
    };

    if (params.args.queryString.date) {
        filter.date = params.args.queryString.date
    }
    const notary: any = await Notary.findOne({
        where: {
            uuid: params.args.params.uuid,
            deletedAt: null
        },
        attributes: { exclude: ["deletedAt", "createdAt", "updatedAt"] },
        include: [{
            attributes: { exclude: ["deletedAt", "createdAt", "updatedAt"] },
            model: EventDuration,
            where: {
                deletedAt: null
            },
        }]
    });

    if (!notary) {
        throw new HttpBadRequest("Notary not found.");
    }

    return {
        message: "OK",
        payload: notary
    }
}