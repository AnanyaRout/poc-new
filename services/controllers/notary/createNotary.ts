import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { ICreateNotary } from "../../interfaces/notary/ICreateNotary";
import { Notary } from "../../../shared/db/models";

export const createNotary = async(params:IControllerParams<ICreateNotary>) => {
    const inputs = params.input;
    const transaction = params.transaction;
    const notary = await Notary.create({
        name: inputs.name,
        email: inputs.email
    },{transaction});

    await transaction.commit();

    return {
        message: "Created",
        payload: notary
    }
}