import { IControllerParams } from "../../../shared/interfaces/IControllerParams";
import { Notary, UserSlots, EventDuration } from "../../../shared/db/models";
import { HttpBadRequest } from "../../../shared/lib/exceptions/HttpBadRequest";

export const notaryWorkSheet = async(params:IControllerParams<{}>) => {
    const details = await Notary.findOne({
        where: {
            uuid: params.args.params.uuid,
        },
        include: [EventDuration,UserSlots]
    });
    if (!details) {
        throw new HttpBadRequest("Notary not found.");
    }
    return {
        message: "Successful",
        payload: details
    }
}