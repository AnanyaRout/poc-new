import * as aws from "aws-sdk";

export const s3 = new aws.S3({
    accessKeyId: process.env.ACCESS_KEY,
    region: process.env.REGION,
    secretAccessKey: process.env.ACCESS_SECRET,
});
