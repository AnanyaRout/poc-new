import * as Ajv from "ajv";
import * as normalize from "ajv-error-messages";
import { ValidationError } from "../exceptions/ValidationError";
import { registerKeywords } from "./registerKeyWords";
import * as Error from "ajv-errors";

const ajv = new Ajv({ allErrors: true, $data: true });
Error(ajv);
// Register custom key words
registerKeywords(ajv);

/**
 * Validate given json against a schema
 * @param schema
 * @param data
 */
export const validate = async (schema: object, data: object): Promise<boolean> => {
    const test = ajv.compile(schema);
    try {
        if (!await test(data)) {
            throw new ValidationError("Validation Error", formatErrors(test.errors));
        }
    } catch (e) {
        e.errors.map((err) => {
            if (err.keyword === "required") {
            err.dataPath = err.params.missingProperty;
            }else if (err.keyword === "errorMessage") {
                err.dataPath = err.dataPath.replace("/", "");
            }
        });
        throw new ValidationError("Validation Error", formatErrors(e.errors));
    }
    return true;
};

const formatErrors = (errors: any[]) => {
    const errorsList = normalize(errors);
    if (Object.keys(errorsList.fields).length) {
        return Object.keys(errorsList.fields).reduce((acc, key) => {
            acc[key] = errorsList.fields[key][0];
            return acc;
        }, {});
    }
    return {};
};
