import Ajv from "ajv";
import * as db from "../../db/models";

export const isUnique = async (schema: any, value: string) => {
    return !(await query(schema.model, { [schema.field]: value }));
};

export const exists = async (schema: any, value: string) => {

    let conditions = {
        [schema.field]: value,
    };

    if (schema.extra) {
        conditions = {
            ...conditions,
            ...schema.extra,
        };
    }
    return await query(schema.model, conditions);
};

// tslint:disable-next-line: only-arrow-functions
export const isValid = async function() {

    const schema: any = arguments[0];
    const value: string = arguments[1];
    const data: string = arguments[6];

    const search = schema.search;
    let conditions = {
        [schema.field]: value,
        [schema.searchField]: data[search],
    };

    if (schema.extra) {
        conditions = {
            ...conditions,
            ...schema.extra,
        };
    }

    return await query(schema.model, conditions);
};


// tslint:disable-next-line: only-arrow-functions
export const isNotDuplicate = async function() {

    const schema: any = arguments[0];
    const value: string = arguments[1];
    const data: string = arguments[6];

    const search = schema.search;
    let conditions = {
        [schema.field]: value,
        [schema.searchField]: data[search],
    };

    if (schema.extra) {
        conditions = {
            ...conditions,
            ...schema.extra,
        };
    }

    return !(await query(schema.model, conditions));
};

const query = async (model: string, conditions: { [key: string]: any }) => {
    return await db[model].findOne({
        where: conditions,
    });
};

export const registerKeywords = (ajv: Ajv.Ajv) => {
    ajv.addKeyword("isUnique", {
        async: true,
        type: "string",
        validate: isUnique,
    });

    ajv.addKeyword("exists", {
        async: true,
        type: "string",
        validate: exists,
    });

    ajv.addKeyword("isValid", {
        $data: true,
        async: true,
        type: "string",
        validate: isValid,
    });

    ajv.addKeyword("isNotDuplicate", {
        $data: true,
        async: true,
        type: "string",
        validate: isNotDuplicate,
    });


};
