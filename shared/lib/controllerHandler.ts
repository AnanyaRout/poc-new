import { Request, Response } from "express";
import { Transaction } from "sequelize";
import { IControllerHandlerParams } from "../interfaces/IControllerHandlerParams";
import { handleError } from "./handleError";
import { logger } from "./logger";
import { Created, Ok } from "./response";
import { validate } from "./validator";
import { getTransaction } from "./generateControllerParams";

/**
 * Controller handler method which validates data with defined schema, call controller and
 * handles the exception.
 * @param param
 */
export const controllerHandler = ({ schema, controller, hasPermission, options }: IControllerHandlerParams) => {
    return async (req: Request, res: Response) => {
        // tslint:disable-next-line:prefer-const
        let transaction: Transaction;

        console.log(hasPermission);
        try {
            const user = {};

            let payload = {};
            payload = { ...(req.body ? req.body : {}), ...req.params };
            if (req.user) {
                // tslint:disable-next-line: no-string-literal
                user["uuid"] = req.user.uuid;
                // tslint:disable-next-line: no-string-literal
                user["role"] = req.user.role && req.user.role;
            }

            // Validate the user input
            if (schema) {
                await validate(schema, payload);
            }
            let createTransaction = true;

            if (options && options.hasOwnProperty("transaction") && options.transaction === false) {
                createTransaction = false;
            }

            if (createTransaction) {
                transaction = await getTransaction();
            }
            const params = {
                args: {
                    params: req.params,
                    queryString: req.query,
                },
                input: payload,
                transaction,
                user,
                req,
            };

            const response = await controller(params);

            // Return response to the client
            const method = req.method === "POST" && response.created ? Created : Ok;
            method(res, response.message, response.payload);
        } catch (e) {
            if (transaction) {
                await transaction.rollback();
            }
            console.log("...............................................................................................................................................................................................................",e);
            logger.error(e); // Log the error for debugging purpose
            handleError(e, res);
        }
    };
};
