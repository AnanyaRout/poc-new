import { DataTypes, Model } from "sequelize";
import { makeModelOptions } from "../../helpers/makeModelOptions";
import { sequelize } from "../sequelize";

class EventDuration extends Model {
    public uuid?: string;
    public eventName: string;
    public duration: number;
    public notaryUuid: string;

    public readonly createdAt: Date;
    public readonly updatedAt: Date;
}
EventDuration.init({
    uuid: {
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        type: DataTypes.UUID,
    },
    eventName: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    duration: {
        allowNull: false,
        type: DataTypes.INTEGER,
    },
    notaryUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    }
}, makeModelOptions(sequelize, "event_duration"));
export default EventDuration;
