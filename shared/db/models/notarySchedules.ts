import { DataTypes, Model } from "sequelize";
import { makeModelOptions } from "../../helpers/makeModelOptions";
import { sequelize } from "../sequelize";

class NotarySchedules extends Model {
    public uuid?: string;
    public day: string;
    public notaryUuid: string;
    public eventUuid: string;
    public startTime: string;
    public endTime: string;

    public readonly createdAt: Date;
    public readonly updatedAt: Date;
}
NotarySchedules.init({
    uuid: {
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        type: DataTypes.UUID,
    },
    startTime: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    endTime: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    day: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    notaryUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    },
    eventUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    }
}, makeModelOptions(sequelize, "notary_schedule"));
export default NotarySchedules;
