import { DataTypes, Model } from "sequelize";
import { makeModelOptions } from "../../helpers/makeModelOptions";
import { sequelize } from "../sequelize";

class Notary extends Model {
    public uuid?: string;
    public name: string;
    public email: string;

    public readonly createdAt: Date;
    public readonly updatedAt: Date;
}
Notary.init({
    uuid: {
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        type: DataTypes.UUID,
    },
    name: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    email: {
        allowNull: false,
        type: DataTypes.STRING,
    }
}, makeModelOptions(sequelize, "notary"));
export default Notary;
