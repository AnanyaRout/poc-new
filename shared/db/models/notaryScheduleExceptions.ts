import { DataTypes, Model } from "sequelize";
import { makeModelOptions } from "../../helpers/makeModelOptions";
import { sequelize } from "../sequelize";

class NotaryScheduleExceptions extends Model {
    public uuid?: string;
    // public startTime: string;
    public dateTime: Date;
    public notaryUuid: string;
    public eventUuid: string;
    // public unavailableOnThisTime: boolean;
    public availableOnThisDate: boolean;

    public readonly createdAt: Date;
    public readonly updatedAt: Date;
}
NotaryScheduleExceptions.init({
    uuid: {
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        type: DataTypes.UUID,
    },
    dateTime: {
        allowNull: false,
        type: DataTypes.DATE,
    },
    availableOnThisDate: {
        type: DataTypes.BOOLEAN,
    },
    notaryUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    },
    eventUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    }
}, makeModelOptions(sequelize, "notary_schedule_exceptions"));
export default NotaryScheduleExceptions;
