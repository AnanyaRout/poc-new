import { DataTypes, Model } from "sequelize";
import { makeModelOptions } from "../../helpers/makeModelOptions";
import { sequelize } from "../sequelize";

class UserSlots extends Model {
    public uuid?: string;
    public dateTime: Date;
    public userUuid: string;
    public notaryUuid: string;
    public eventUuid: string;

    public readonly createdAt: Date;
    public readonly updatedAt: Date;
}
UserSlots.init({
    uuid: {
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
        type: DataTypes.UUID,
    },
    dateTime: {
        allowNull: false,
        type: DataTypes.DATE,
    },
    userUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    },
    notaryUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    },
    eventUuid: {
        allowNull: false,
        type: DataTypes.UUID,
    }
}, makeModelOptions(sequelize, "user_slots"));
export default UserSlots;
