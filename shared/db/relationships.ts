import { NotaryScheduleExceptions, User, UserSlots, EventDuration } from "./models";
import Notary from "./models/notary";
import NotarySchedules from "./models/notarySchedules";

// Days.belongsTo(Notary, { foreignKey: "notaryUuid", onDelete: "restrict" });
// Notary.hasMany(Days, { foreignKey: "notaryUuid", onDelete: "restrict" });

UserSlots.belongsTo(User, { foreignKey: "userUuid", onDelete: "restrict" });
User.hasMany(UserSlots, { foreignKey: "userUuid", onDelete: "restrict" });

UserSlots.belongsTo(EventDuration, { foreignKey: "eventUuid", onDelete: "restrict" });
EventDuration.hasMany(UserSlots, { foreignKey: "eventUuid", onDelete: "restrict" });

// UserSlots.belongsTo(Days, { foreignKey: "dayUuid", onDelete: "restrict" });
// Days.hasMany(UserSlots, { foreignKey: "dayUuid", onDelete: "restrict" });

// NotarySchedules.hasMany(Days, { foreignKey: "notarySchedulesUuid", onDelete: "restrict" });
// Days.belongsTo(NotarySchedules, { foreignKey: "notarySchedulesUuid", onDelete: "restrict" });

// NotaryScheduleExceptions.belongsTo(Notary, { foreignKey: "notaryUuid", onDelete: "restrict" });
// Notary.hasMany(NotaryScheduleExceptions, { foreignKey: "notaryUuid", onDelete: "restrict" });

NotaryScheduleExceptions.belongsTo(EventDuration, { foreignKey: "eventUuid", onDelete: "restrict" });
EventDuration.hasMany(NotaryScheduleExceptions, { foreignKey: "eventUuid", onDelete: "restrict" });

EventDuration.belongsTo(Notary, { foreignKey: "notaryUuid", onDelete: "restrict" });
Notary.hasMany(EventDuration, { foreignKey: "notaryUuid", onDelete: "restrict" });

NotarySchedules.belongsTo(EventDuration, { foreignKey: "eventUuid", onDelete: "restrict" });
EventDuration.hasMany(NotarySchedules, { foreignKey: "eventUuid", onDelete: "restrict" });
