import * as mysql from "mysql2";
import { Options, Sequelize } from "sequelize";

// Options for the database connection
const options: Options = {
    database: process.env.DB_SCHEMA,
    dialect: "mysql",
    dialectModule: mysql,
    host: process.env.DB_HOST,
    username: process.env.DB_USERNAME,
};
/**
 * Add password to the options if it is provided
 * This is to prevent mysql throwing errors "PASSWORD=YES" when
 * running on environments which does not have password like localhost
 */
if (process.env.DB_PASSWORD !== "null") {
    options.password = process.env.DB_PASSWORD;
}

// Export a singleton connection instance
export const sequelize = new Sequelize(options);
